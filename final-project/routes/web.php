<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TopicController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/questions');
});

Route::get('/home', function () {
    return view('pages/home');
});

Route::resource('topic', TopicController::class)->except(['show']);

Route::get('/generate-topics-pdf', [PdfController::class, 'generatePdf'])->name('generate_topics_pdf');

Route::resource('questions', QuestionController::class);

Route::group(['prefix' => 'answers'], function () {
    Route::post('/{id}', [AnswerController::class, 'store']);
    Route::get('/{id}/edit', [AnswerController::class, 'edit']);
    Route::put('/{id}', [AnswerController::class, 'update']);
    Route::delete('/{id}', [AnswerController::class, 'destroy'])->name('answers.destroy');
});

Route::resource('users', UserController::class);

Route::middleware(['auth'])->group(function () {
    Route::resource('profile', ProfileController::class)->only(['edit', 'update']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
