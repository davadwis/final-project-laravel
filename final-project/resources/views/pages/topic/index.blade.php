@extends('layout/base')

@push('styles')
<link href="{{asset('/template/plugins/tables/css/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<!-- Sweetalret -->
<link href="{{asset('/template/plugins/sweetalert/css/sweetalert.css')}}" rel="stylesheet">
@endpush

@section('content')

<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between px-3">
                <h4 class="card-title">Data Topic</h4>
                <div>
                    <a href="/topic/create" class="btn btn-primary my-2 px-3">Create</a>
                    <a href="{{route('generate_topics_pdf')}}" class="btn btn-info my-2 px-3">Export PDF</a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered zero-configuration rounded">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($topic as $index => $topic)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $topic -> name }}</td>
                            <td>
                                <form action="/topic/{{$topic->id}}" method="post" class="d-flex">
                                    <a href="/topic/{{$topic->id}}/edit" class="btn btn-warning btn-sm mr-2">Edit</a>
                                    @csrf
                                    @method("delete")
                                    <div class="sweetalert">
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm sweet-success-cancel">
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr class="text-center">
                            <td></td>
                            <td>No Data Available</td>
                            <td></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{asset('/template/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/template/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('/template/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>
<!-- Sweetalert -->
<script src="{{asset('/template/plugins/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/template/plugins/sweetalert/js/sweetalert.init.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.sweet-success-cancel').on('click', function(event) {
            event.preventDefault();
            var form = $(this).closest('form');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        swal("Deleted!", "The topic has been deleted!", "success");
                        form.submit();
                    } else {
                        swal("Cancelled", "The topic is safe :)", "error");
                    }
                });
        });
    });
</script>
@endpush