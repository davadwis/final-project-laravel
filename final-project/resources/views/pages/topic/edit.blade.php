@extends('layout/base')

@section('content')

<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-body">
            <div class="basic-form">
                <form action="/topic/{{$topic -> id}}" method="post">
                    @csrf
                    @method('put')

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="name">Topik</label>
                        <input type="text" class="form-control input-flat rounded" placeholder="Masukkan Topik Baru" name="name" value="{{$topic -> name}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection