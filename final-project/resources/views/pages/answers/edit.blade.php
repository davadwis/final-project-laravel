@extends('layout.base')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <form action="/answers/{{$answers->id}}" method="post">
                @csrf
                @method('put')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div>
                    @foreach ($questions as $item)
                            <input type="hidden" value="{{ $item->id }}" name="question_id">
                        @endforeach
                </div>
                <div class="form-group">
                    <label for="content">Answer:</label>
                    <textarea class="form-control rounded" name="content" rows="5" placeholder="Give your answer ...">{{$answers->content}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Edit Answer</button>
            </form>
        </div>
    </div>
</div>
@endsection