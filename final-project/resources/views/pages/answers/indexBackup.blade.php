@extends('layout.base')

@section('content')
    <div class="container-fluid">
        {{-- <div class="row mx-2">
            @forelse ($questions as $question)
                <div class="col-12">
                    <div class="card p-5">
                        <div class="form-group">
                            <h3>QUESTION</h3>
                        </div>
                        <div class="text-center">
                            <img class="card-img-top rounded" src="{{ asset('image/' . $question->thumbnail) }}"
                                alt="{{ $question->title }}" style="height: 350px;width:min-content;">
                        </div>
                        <div class="card-body pb-1">
                            <p class="card-text">{{ $question->content }}</p>
                        </div>
                    </div>
                </div>
            @empty
                <p>kosong</p>
            @endforelse --}}
            <div class="row mx-2">
                @forelse ($answers as $answer)
                    <div class="col-12">
                        <div class="card p-2">
                            <h3>{{ $answer->content }}</h3>
                            @auth
                                @if ($answer->user_id === Auth::user()->id)
                                    <div class="row my-2 gap-2">
                                        <div class="col">
                                            <a href="/questions/{{ $answer->id }}/edit"
                                                class="btn btn-warning btn-block btn-sm">Edit</a>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-danger btn-block btn-sm delete-btn"
                                                data-toggle="modal" data-target="#deleteModal{{ $answer->id }}">
                                                Delete
                                            </button>
                                        </div>
                                        {{-- <div class="col">
                                            <form action="/questions/{{ $question->id }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                                            </form>
                                        </div> --}}
                                    </div>
                                @endif
                            @endauth
                        </div>
                    </div>
            </div>
        @empty
            <h2>No Answers Available</h2>
            @endforelse
        </div>
    </div>
@endsection
