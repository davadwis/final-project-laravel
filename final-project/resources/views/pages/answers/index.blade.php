@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="ml-4 mb-3">
            <a href="/answers/create" class="btn btn-primary btn-sm px-3 mb-2">Add New Answer</a>
        </div>
        <div class="row mx-2">
            @forelse ($answers as $answer)
                <div class="col-md-6 col-lg-4">
                    <div class="card p-2">
                        <h2 class="card-text ">{{ $answer->content }}</h2>
                        <p>Posted by: {{ $answer->user->username }}</p>
                        <p>{{ $answer->created_at->ago() }}</p>

                        @auth
                            @if ($answer->user_id === Auth::user()->id)
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group dropup">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        </button>
                                        <div class="dropdown-menu">
                                            <div class="col mb-1">
                                                <a href="/answers/{{ $answer->id }}/edit"
                                                    class="btn btn-warning btn-block btn-sm">Edit</a>
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-danger btn-block btn-sm delete-btn"
                                                    data-toggle="modal" data-target="#deleteAnswerModal{{ $answer->id }}">
                                                    Delete
                                                </button>
                                            </div>

                                        </div>
                                        <x-modal modalId="deleteAnswerModal{{ $answer->id }}" aria-labelledby="deleteAnswerModalLabel"
                                            action-route="{{ route('answers.destroy', $answer->id) }}"
                                            modal-title="Confirm Delete"
                                            modal-body="Are you sure you want to delete this question?"
                                            button-name="Delete"></x-modal>
                                    </div>
                                </div>
                            @endif
                        @endauth
                    </div>
                @empty
                    <h2>No Question Available</h2>
            @endforelse
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
