@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <form action="/answers" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        @foreach ($questions as $item)
                            <input type="hidden" value="{{ $item->id }}" name="question_id">
                        @endforeach
                        <div class="form-group">
                            <label for="content">Answer</label>
                            <textarea class="form-control" name="content" rows="5" placeholder="Ketikan sesuatu .."></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
