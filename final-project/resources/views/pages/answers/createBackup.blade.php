@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="row mx-2">
            @forelse ($questions as $question)
                <div class="col-12">
                    <div class="card p-5">
                        <div class="form-group">
                            <h3>QUESTION</h3>
                        </div>
                        <div class="text-center">
                            <img class="card-img-top rounded" src="{{ asset('image/' . $question->thumbnail) }}"
                                alt="{{ $question->title }}" style="height: 350px;width:min-content;">
                        </div>
                        <div class="card-body pb-1">
                            <p class="card-text">{{ $question->content }}</p>
                        </div>
                    </div>
                </div>
                @empty
                <p>kosong</p>
                @endforelse
        </div>
        <div class="card">
        <div class="card-body">

        <form action="/answers" method="POST" enctype="multipart/form-data">
            @csrf
            @if ($errors->  any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                    @foreach ($questions as $item)
                    <input type="hidden" value="{{$item->id}}" name="question_id">
                    @endforeach
            <div class="form-group">
                <label for="content">Answer</label>
                <textarea class="form-control" name="content" rows="5" placeholder="Ketikan sesuatu .."></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
