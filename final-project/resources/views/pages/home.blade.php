@extends('layout/base')

@section('content')
        <form action="/home" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" name="nama" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Umur</label>
                <input type="number" class="form-control" name="umur">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Bio</label>
                <textarea name="bio" class="form-control" name="bio"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
            </form>
@endsection
