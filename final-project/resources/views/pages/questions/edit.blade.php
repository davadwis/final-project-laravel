@extends('layout/base')

@section('content')
<div class="container-fluid ">
    <div class="card">
        <div class="card-body">
            <div class="basic-fo rm">
                <form action="/questions/{{$question->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="topic">Topic</label>
                        <select name="topic_id" class="form-control form-control-sm">
                            <option>--Pilih Topic--</option>
                            @forelse ($topics as $topic)
                            @if ($topic->id === $question->topic_id)
                            <option value="{{$topic->id}}" selected>{{$topic->name}}</option>
                            @else
                            <option value="{{$topic->id}}">{{$topic->name}}</option>
                            @endif
                            @empty
                            <option value="">No Data</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="content">Questions</label>
                        <textarea class="form-control" name="content" rows="5" placeholder="Ketikan sesuatu ..">{{$question->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="formFile" class="form-label">File</label>
                        <input class="form-control input-flat rounded" type="file" name="thumbnail">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection