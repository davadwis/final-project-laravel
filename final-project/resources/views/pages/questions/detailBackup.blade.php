@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h2 class="card-title">Questions</h4>
                </div>
                <div class="form-group">
                    <a href="/questions/create" class="btn btn-primary my-2 px-3">Create</a>
                </div>
                <div class="mb-3">
                    @forelse ($questions as $item)
                        <a>{{$item->topic}}</a>
                        <h4>{{ $item->content }}</h4>
                            <a>{{ $item->thumbnail }}</a>

                        @empty
                            <h3>Tidak ada Pertanyaan</h3>
                    @endforelse
                </div>
            </div>
        </div>
    @endsection
