@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="mb-3 d-flex">
                    <div class="text-center">
                        <img class="card-img-top rounded img-fluid" src="{{ asset('image/' . $question->thumbnail) }}"
                            alt="img_thumbnail" style="height: 350px;width:min-content;">
                    </div>
                    <div class="ml-5 flex-grow-1">
                        <h3>{{ $question->content }}</h3>
                        <p>Posted by: {{ $question->user->username }}</p>
                        <form action="/answers/{{ $question->id }}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="content">Answer:</label>
                                <textarea class="form-control rounded" name="content" rows="5" placeholder="Give your answer ..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Add Answer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- Comment Section --}}
        @if ($answers->count() > 0)
            <p style="font-size: 20px; font-weight: 700">All answers ({{ $answers->count() }})</p>
        @endif
        @forelse ($answers as $answer)
            <div class="card">
                <div class="card-body d-flex justify-content-between">
                    <div>
                        <h3>{{ $answer->content }}</h3>
                        <p>Replied by: {{ $answer->user->username }}</p>
                        <p>{{ $answer->created_at->ago() }}</p>
                    </div>
                    @auth
                        @if ($answer->user->id === Auth::user()->id)
                            <div class="d-flex justify-content-end">
                                <div class="btn">
                                    <button type="button" class="btn btn-secondary btn-xs button-none" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-three-dots" viewBox="0 0 16 16">
                                            <path
                                                d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                                        </svg>
                                    </button>
                                    <div class="dropdown-menu">
                                        <div class="col mb-1">
                                            <a href="/answers/{{ $answer->id }}/edit"
                                                class="btn btn-warning btn-block btn-sm">Edit</a>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-danger btn-block btn-sm delete-btn"
                                                data-toggle="modal" data-target="#deleteAnswerModal{{ $answer->id }}">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                    <x-modal modalId="deleteAnswerModal{{ $answer->id }}"
                                        aria-labelledby="deleteAnswerModalLabel"
                                        action-route="{{ route('answers.destroy', $answer->id) }}" modal-title="Confirm Delete"
                                        modal-body="Are you sure you want to delete this question?"
                                        button-name="Delete"></x-modal>

                                </div>
                            </div>
                        @endif
                    @endauth
                </div>
            </div>
        @empty
            <div class="card p-4">
                <p style="font-size: 20px" class="text-center pb-0">No answers for this Post</p>
            </div>
        @endforelse
    </div>

    </div>
@endsection
