@extends('layout/base')

@section('content')
<div class="container-fluid ">
    <div class="card">
        <div class="card-body">
            <div class="basic-fo rm">
                <form action="/questions" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="topic">Topic</label>
                        <select name="topic_id" class="form-control form-control-sm">
                            <option>--Pilih Topic--</option>
                            @foreach ($topics as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="content">Questions</label>
                        <textarea class="form-control" name="content" rows="5" placeholder="Ketikan sesuatu .."></textarea>
                    </div>
                    <div class="form-group">
                        <label for="formFile" class="form-label">File</label>
                        <input class="form-control input-flat rounded" type="file" name="thumbnail">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection