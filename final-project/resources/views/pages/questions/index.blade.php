@extends('layout.base')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <div class="form-group">
                        <h1>What do you want to post or share?</h1>
                    </div>
                    <a href="/questions/create" type="submit" class="btn btn-primary"><i
                            class="fa fa-pencil color-muted"></i><span class="ml-3">Post</span></a>
                </div>
            </div>
        </div>

        @if ($search)
            <p class="mb-4" style="font-size: 24px; font-weight: 700">Result for: {{ $search }}</p>
        @endif
        <div class="row mx-2">
            @forelse ($questions as $question)
                <div class="col-md-6 col-lg-4">
                    <div class="card p-2">
                        <div class="text-center">
                            <img class="card-img-top rounded img-fluid" src="{{ asset('image/' . $question->thumbnail) }}"
                                alt="{{ $question->title }}" style="height: 350px;width:min-content;">
                        </div>
                        <div class="card-body p-2 mt-1">
                            <span class="badge badge-info p-1 mb-2">{{ $question->topic->name }}</span>
                            <h2 class="card-text ">{{ $question->content }}</h2>
                            <div class="d-flex justify-content-between align-items-center">
                                <p>Posted by: {{ $question->user->username }}</p>
                                <p>{{ $question->created_at->ago() }}</p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <a href="/answers/create" class="pe-auto" style="cursor: pointer;" data-toggle="tooltip"
                                        data-placement="top" title="Reply this post">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                fill="currentColor" class="bi bi-chat" viewBox="0 0 16 16">
                                                <path
                                                    d="M2.678 11.894a1 1 0 0 1 .287.801 11 11 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8 8 0 0 0 8 14c3.996 0 7-2.807 7-6s-3.004-6-7-6-7 2.808-7 6c0 1.468.617 2.83 1.678 3.894m-.493 3.905a22 22 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a10 10 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9 9 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105" />
                                            </svg>
                                        </div>
                                    </a>
                                    <a href="/questions/{{ $question->id }}">
                                        <span class="ml-2">{{ $question->answers->count() }} answers</span>
                                    </a>
                                </div>
                                @auth
                                    @if ($question->user_id === Auth::user()->id)
                                        <div class="d-flex justify-content-end align-items-center">
                                            <div>
                                                <button type="button" class="btn btn-secondary btn-xs button-none" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                        fill="currentColor" class="bi bi-three-dots" viewBox="0 0 16 16">
                                                        <path
                                                            d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3" />
                                                    </svg>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <div class="col mb-1">
                                                        <a href="/questions/{{ $question->id }}/edit"
                                                            class="btn btn-warning btn-block btn-sm">Edit</a>
                                                    </div>
                                                    <div class="col">
                                                        <button type="button"
                                                            class="btn btn-danger btn-block btn-sm delete-btn"
                                                            data-toggle="modal" data-target="#deleteModal{{ $question->id }}">
                                                            Delete
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <x-modal modalId="deleteModal{{ $question->id }}" aria-labelledby="deleteModalLabel"
                                            action-route="{{ route('questions.destroy', $question->id) }}"
                                            modal-title="Confirm Delete"
                                            modal-body="Are you sure you want to delete this question?"
                                            button-name="Delete"></x-modal>
                                    @endif
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                @if ($search)
                    <p style="font-size: 18px">Cannot found search with value '{{ $search }}'</p>
                @else
                    <p style="font-size: 18px">No question available</p>
                @endif
            @endforelse
        </div>
        @if (count($questions) > 0)
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item {{ $questions->currentPage() == 1 ? 'disabled' : '' }}">
                        <a class="page-link" href="{{ $questions->previousPageUrl() }}" tabindex="-1">Previous</a>
                    </li>
                    @for ($i = 1; $i <= $questions->lastPage(); $i++)
                        <li class="page-item {{ $questions->currentPage() == $i ? 'active' : '' }}">
                            <a class="page-link" href="{{ $questions->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor
                    <li class="page-item {{ $questions->currentPage() == $questions->lastPage() ? 'disabled' : '' }}">
                        <a class="page-link" href="{{ $questions->nextPageUrl() }}">Next</a>
                    </li>
                </ul>
            </nav>
        @endif
    </div>
@endsection
