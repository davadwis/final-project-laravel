@extends('layout/base')

@section('content')
    <div class="container-fluid ">
        <h1>Update Profile</h1>
        <div class="card mt-3">
            <div class="card-body">
                <div class="basic-fo rm">
                    <form action="/profile/{{$profileDetail->user->id}}/detail" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="alamat">Username</label>
                            <input type="text" class="form-control" value="{{$profileDetail->user->username}}" id="username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Age</label>
                            <input type="text" class="form-control" value="{{$profileDetail->age}}" id="age" name="age">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Bio</label>
                            <textarea class="form-control" name="bio" id="" cols="30" rows="10" id="bio" name="bio">{{$profileDetail->bio}}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
