@extends('layout.base')

@section('content')
<div class="container-fluid ">
    <h1>Profile</h1>
    <div class="card mt-3">
        <div class="card-body">
            <div class="basic-fo rm">
                    <div class="form-group">
                        <h3 for="alamat">Username</h3>
                        <a>{{$profileDetail->user->username}}</a>
                    </div>
                    <div class="form-group">
                        <h3 for="alamat">Age</h3>
                        <a>{{$profileDetail->age}}</a>
                    </div>
                    <div class="form-group">
                        <h3 for="alamat">Bio</h3>
                        <a>{{$profileDetail->bio}}</a>
                    </div>
                    <div>
                        <a href="/profile/{{$profileDetail->user->id}}/index" class="btn btn-primary btn-sm px-3 mb-2">Edit</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection