@props(['modalId', 'ariaLabelledby', 'modalTitle', 'modalBody', 'actionRoute', 'buttonName'])

<div class="modal fade" id="{{$modalId}}" tabindex="-1" role="dialog" aria-labelledby="{{$ariaLabelledby}}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="{{$ariaLabelledby}}">{{$modalTitle}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>{{$modalBody}}</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              {{-- Add your delete route --}}
              <form action="{{$actionRoute}}" method="post">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger">{{$buttonName}}</button>
              </form>
          </div>
      </div>
  </div>
</div>