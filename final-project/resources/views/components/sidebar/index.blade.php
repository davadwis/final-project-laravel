<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li><a href="{{asset('/topic')}}">Topic</a></li>
            <li><a href="{{asset('/questions')}}">Question</a></li>
            <!-- <li><a href="{{asset('/answers')}}">Answers</a></li> -->

            {{-- <li class="mega-menu mega-menu-sm">
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Layouts</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{asset('/templatelayout-blank.html')}}">Blank</a></li>
            <li><a href="{{asset('/template/layout-one-column.html')}}">One Column</a></li>
            <li><a href="{{asset('/template/layout-two-column.html')}}">Two column</a></li>
            <li><a href="{{asset('/template/layout-compact-nav.html')}}">Compact Nav</a></li>
            <li><a href="{{asset('/template/layout-vertical.html')}}">Vertical</a></li>
            <li><a href="{{asset('/template/layout-horizontal.html')}}">Horizontal</a></li>
            <li><a href="{{asset('/template/layout-boxed.html')}}">Boxed</a></li>
            <li><a href="{{asset('/template/layout-wide.html')}}">Wide</a></li>


            <li><a href="{{asset('/template/layout-fixed-header.html')}}">Fixed Header</a></li>
            <li><a href="{{asset('/template/layout-fixed-sidebar.html')}}">Fixed Sidebar</a></li>
        </ul>
        </li>
        <li class="nav-label">Apps</li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-envelope menu-icon"></i> <span class="nav-text">Email</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/email-inbox.html')}}">Inbox</a></li>
                <li><a href="{{asset('/template/email-read.html')}}">Read</a></li>
                <li><a href="{{asset('/template/email-compose.html')}}">Compose</a></li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-screen-tablet menu-icon"></i><span class="nav-text">Apps</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/app-profile.html')}}">Profile</a></li>
                <li><a href="{{asset('/template/app-calender.html')}}">Calender</a></li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-graph menu-icon"></i> <span class="nav-text">Charts</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/chart-flot.html')}}">Flot</a></li>
                <li><a href="{{asset('/template/chart-morris.html')}}">Morris</a></li>
                <li><a href="{{asset('/template/chart-chartjs.html')}}">Chartjs</a></li>
                <li><a href="{{asset('/template/chart-chartist.html')}}">Chartist</a></li>
                <li><a href="{{asset('/template/chart-sparkline.html')}}">Sparkline</a></li>
                <li><a href="{{asset('/template/chart-peity.html')}}">Peity</a></li>
            </ul>
        </li>
        <li class="nav-label">UI Components</li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-grid menu-icon"></i><span class="nav-text">UI Components</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/ui-accordion.html')}}">Accordion</a></li>
                <li><a href="{{asset('/template/ui-alert.html')}}">Alert</a></li>
                <li><a href="{{asset('/template/ui-badge.html')}}">Badge</a></li>
                <li><a href="{{asset('/template/ui-button.html')}}">Button</a></li>
                <li><a href="{{asset('/template/ui-button-group.html')}}">Button Group</a></li>
                <li><a href="{{asset('/template/ui-cards.html')}}">Cards</a></li>
                <li><a href="{{asset('/template/ui-carousel.html')}}">Carousel</a></li>
                <li><a href="{{asset('/template/ui-dropdown.html')}}">Dropdown</a></li>
                <li><a href="{{asset('/template/ui-list-group.html')}}">List Group</a></li>
                <li><a href="{{asset('/template/ui-media-object.html')}}">Media Object</a></li>
                <li><a href="{{asset('/template/ui-modal.html')}}">Modal</a></li>
                <li><a href="{{asset('/template/ui-pagination.html')}}">Pagination</a></li>
                <li><a href="{{asset('/template/ui-popover.html')}}">Popover</a></li>
                <li><a href="{{asset('/template/ui-progressbar.html')}}">Progressbar</a></li>
                <li><a href="{{asset('/template/ui-tab.html')}}">Tab</a></li>
                <li><a href="{{asset('/template/ui-typography.html')}}">Typography</a></li>
                <!-- </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="icon-layers menu-icon"></i><span class="nav-text">Components</span>
                        </a>
                        <ul aria-expanded="false"> -->
                <li><a href="{{asset('/template/uc-nestedable.html')}}">Nestedable</a></li>
                <li><a href="{{asset('/template/uc-noui-slider.html')}}">Noui Slider</a></li>
                <li><a href="{{asset('/template/uc-sweetalert.html')}}">Sweet Alert</a></li>
                <li><a href="{{asset('/template/uc-toastr.html')}}">Toastr</a></li>
            </ul>
        </li>
        <li>
            <a href="widgets.html" aria-expanded="false">
                <i class="icon-badge menu-icon"></i><span class="nav-text">Widget</span>
            </a>
        </li>
        <li class="nav-label">Forms</li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-note menu-icon"></i><span class="nav-text">Forms</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/form-basic.html')}}">Basic Form</a></li>
                <li><a href="{{asset('/template/form-validation.html')}}">Form Validation</a></li>
                <li><a href="{{asset('/template/form-step.html')}}">Step Form</a></li>
                <li><a href="{{asset('/template/form-editor.html')}}">Editor</a></li>
                <li><a href="{{asset('/template/form-picker.html')}}">Picker</a></li>
            </ul>
        </li>
        <li class="nav-label">Table</li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-menu menu-icon"></i><span class="nav-text">Table</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/table-basic.html')}}" aria-expanded="false">Basic Table</a></li>
                <li><a href="{{asset('/template/table-datatable.html')}}" aria-expanded="false">Data Table</a></li>
            </ul>
        </li>
        <li class="nav-label">Pages</li>
        <li>
            <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                <i class="icon-notebook menu-icon"></i><span class="nav-text">Pages</span>
            </a>
            <ul aria-expanded="false">
                <li><a href="{{asset('/template/page-login.html')}}">Login</a></li>
                <li><a href="{{asset('/template/page-register.html')}}">Register</a></li>
                <li><a href="{{asset('/template/page-lock.html')}}">Lock Screen</a></li>
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Error</a>
                    <ul aria-expanded="false">
                        <li><a href="{{asset('/template/page-error-404.html')}}">Error 404</a></li>
                        <li><a href="{{asset('/template/page-error-403.html')}}">Error 403</a></li>
                        <li><a href="{{asset('/template/page-error-400.html')}}">Error 400</a></li>
                        <li><a href="{{asset('/template/page-error-500.html')}}">Error 500</a></li>
                        <li><a href="{{asset('/template/page-error-503.html')}}">Error 503</a></li>
                    </ul>
                </li>
            </ul>
        </li> --}}
        </ul>
    </div>
</div>