<!--**********************************
            Nav header start
        ***********************************-->
<div class="nav-header">
    <div class="brand-logo">
        <a href="index.html">
            <b class="logo-abbr"><img src="{{ asset('/template/images/logo.png') }}" alt=""> </b>
            <span class="logo-compact"><img src="{{ asset('/template/images/logo-compact.png') }}" alt=""></span>
            <span class="brand-title">
                <img src="{{ asset('/template/images/logo-text.png') }}" alt="">
            </span>
        </a>
    </div>
</div>
<!--**********************************
            Nav header end
        ***********************************-->

<!--**********************************
            Header start
        ***********************************-->
<div class="header">
    <div class="header-content clearfix">

        <div class="nav-control">
            <div class="hamburger">
                <span class="toggle-icon"><i class="icon-menu"></i></span>
            </div>
        </div>
        <div class="header-left">
            <div class="input-group icons">
                <div class="input-group-prepend">
                    <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                </div>
                <form action="/questions" method="GET">
                    <input type="search" class="form-control" placeholder="Search a post or topic..." aria-label="Search your post or topic" name="search">
                </form>
                <div class="drop-down animated flipInX d-md-none">
                    <form action="#">
                        <input type="text" class="form-control" placeholder="Search">
                    </form>
                </div>
            </div>
        </div>
        <div class="header-right">
            @guest
            <div>
                <a class="btn btn-primary" href="{{ Route('login') }}">Login</a>
            </div>
            @endguest
            @auth
            <ul class="clearfix ">
                <li class="icons">
                    <p class="mb-0">{{ Auth::user()->email }}</p>
                </li>
                <li class="icons dropdown">
                    <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                        <span class="activity active"></span>
                        <img src="{{ asset('/template/images/user/1.png') }}" height="40" width="40" alt="">
                    </div>
                    <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="/profile/{{ Auth::user()->id }}/edit"><i class="icon-user"></i> <span>Profile</span></a>
                                </li>
                                <hr class="my-2">
                                <li>
                                    <a class="dropdown-item p-0" href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                                        <i class="icon-key"></i><span>Logout</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            @endauth
        </div>
    </div>
</div>
<!--**********************************
            Header end ti-comment-alt
        ***********************************-->