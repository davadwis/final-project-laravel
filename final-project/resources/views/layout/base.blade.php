<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Quixlab - Bootstrap Admin Dashboard Template by Themefisher.com</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/template/images/favicon.png')}}">
    <!-- Pignose Calender -->
    <link href="{{asset('/template/plugins/pg-calendar/css/pignose.calendar.min.css')}}" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="{{asset('/template/plugins/chartist/css/chartist.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('/template/plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css')}}">
    <!-- Custom Stylesheet -->
    <link href="{{asset('/template/css/style.css')}}" rel="stylesheet">
    <style>
        .button-none {
            background: none; 
            border: none;
        }
        .button-none:hover {
            color: black;
            background: none;
            border: none;
            opacity: 0.6;
        }
    </style>
    @stack('styles')
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <x-navbar />
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <x-sidebar />
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <section class="content">
                @yield('content')
            </section>

        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{asset('/template/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('/template/js/custom.min.js')}}"></script>
    <script src="{{asset('/template/js/settings.js')}}"></script>
    <script src="{{asset('/template/js/gleek.js')}}"></script>
    <script src="{{asset('/template/js/styleSwitcher.js')}}"></script>

    <!-- Chartjs -->
    <script src="{{asset('/template/plugins/chart.js/Chart.bundle.min.js')}}"></script>
    <!-- Circle progress -->
    <script src="{{asset('/template/plugins/circle-progress/circle-progress.min.js')}}"></script>
    <!-- Datamap -->
    <script src="{{asset('/template/plugins/d3v3/index.js')}}"></script>
    <script src="{{asset('/template/plugins/topojson/topojson.min.js')}}"></script>
    <script src="{{asset('/template/plugins/datamaps/datamaps.world.min.js')}}"></script>
    <!-- Morrisjs -->
    <script src="{{asset('/template/plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('/template/plugins/morris/morris.min.js')}}"></script>
    <!-- Pignose Calender -->
    <script src="{{asset('/template/plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('/template/plugins/pg-calendar/js/pignose.calendar.min.js')}}"></script>
    <!-- ChartistJS -->
    <script src="{{asset('/template/plugins/chartist/js/chartist.min.js')}}"></script>
    <script src="{{asset('/template/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js')}}"></script>

    <script src="{{asset('/template/js/dashboard/dashboard-1.js')}}"></script>

    @stack('scripts')
</body>

</html>