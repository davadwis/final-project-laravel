<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Barryvdh\DomPDF\Facade\Pdf as FacadePdf;

class PdfController extends Controller
{
    public function generatePdf()
    {

        $topics = Topic::all();

        $pdf = FacadePdf::loadView('pdf.test', [
            "topics" => $topics
        ]); 
        return $pdf->download('pdf.test');
    }
}
