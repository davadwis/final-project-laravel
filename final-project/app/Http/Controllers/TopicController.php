<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        $topic = Topic::all();
        // dd($topic);
        return view('pages.topic.index', compact('topic'));
    }

    public function create()
    {
        return view('pages.topic.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:topics'
        ]);

        Topic::create([
            'name' => $request->name,
        ]);

        return redirect('/questions');
    }

    public function edit($id)
    {
        $topic = Topic::find($id);
        return view('pages.topic.edit', compact('topic'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:topics'
        ]);

        $topic = Topic::find($id);
        $topic->name = $request->name;
        $topic->update();
        toastr()->success('Updated successfully!');

        return redirect('/topic');
    }

    public function destroy($id)
    {
        $topic = Topic::find($id);
        $topic->delete();
        if ($topic instanceof Model) {
            toastr()->success('Deleted successfully!');

            return redirect('/topic');
        }

        toastr()->error('An error has occurred please try again later.');
        return redirect('/topic');
    }
}
