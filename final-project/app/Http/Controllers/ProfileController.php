<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function edit($id)
    {
        $profileDetail = Profile::where("user_id", "=", $id)->first();
        return view('pages.profile.edit', compact("profileDetail"));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            "username" => "required",
            "age" => "required",
            "bio" => "required"
        ]);

        $user = User::find($id);

        $user->username = $request['username'];
        $user->save();

        $profile = Profile::find($id);

        $profile->age = $request["age"];
        $profile->bio = $request["bio"];
        $profile->save();


        if ($profile instanceof Model) {
            toastr()->success('Updated successfully!');

            return redirect('/profile/' . $user->id . '/edit');
        }

        toastr()->error('An error has occurred please try again later.');

        return back();
    }
}
