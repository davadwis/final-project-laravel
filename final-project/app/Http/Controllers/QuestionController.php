<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Request $request)
    {

        $search = $request->input('search');
        if ($search) {
            $questions = Question::where('content', "like", '%' . $search . '%')
            ->paginate(5);
        } else {
            $questions = Question::paginate(5);
        }

        return view('pages.questions.index', compact('questions', 'search'));
    }

    public function create()
    {
        $topics = Topic::all();
        return view('pages.questions.create', ['topics' => $topics]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'thumbnail' => 'required|image|mimes:jpg,png,jpeg',
            'topic_id' => 'required',
        ]);

        $filethumbnail = time() . '.' . $request->thumbnail->extension();
        $request->thumbnail->move(public_path('image'), $filethumbnail);

        Question::create([
            'content' => $request->content,
            'thumbnail' => $filethumbnail,
            'topic_id' => $request->topic_id,
            'user_id' => Auth::user()->id,
        ]);

        return redirect('/questions');
    }

    public function show($id)
    {
        $question = Question::find($id);

        $answers = Answer::where("question_id", "=", $question->id)->with('user')->get();
        $topic = Topic::where("id", "=", $question->topic_id)->first();

        // dd($answers[0]->user->username);

        return view('pages.questions.detail', ['question' => $question, 'answers' => $answers, 'topic' => $topic]);
    }

    public function edit($id)
    {
        $question = Question::find($id);
        $topics = Topic::get();

        return view('pages.questions.edit', ['question' => $question, 'topics' => $topics]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'topic_id' => 'required',
        ]);

        $question = Question::find($id);

        // Check if a new thumbnail file is provided
        if ($request->hasFile('thumbnail')) {
            $request->validate([
                'thumbnail' => 'image|mimes:jpg,png,jpeg',
            ]);

            // Delete the old thumbnail file
            $path = 'image/';
            File::delete($path . $question->thumbnail);

            // Upload the new thumbnail file
            $filethumbnail = time() . '.' . $request->file('thumbnail')->extension();
            $request->file('thumbnail')->move(public_path('image'), $filethumbnail);

            // Update the question with the new thumbnail
            $question->thumbnail = $filethumbnail;
        }

        // Update other fields
        $question->content = $request->content;
        $question->topic_id = $request->topic_id;
        $question->user_id = Auth::user()->id;

        // Save the changes
        $question->save();

        toastr()->success('Updated successfully!');

        return redirect('/questions');
    }

    public function destroy($id)
    {
        $question = Question::find($id);

        $path = 'image/';
        File::delete($path . $question->thumbnail);

        $question->answers()->delete();

        $question->delete();

        if ($question instanceof Model) {
            toastr()->success('Deleted successfully!');

            return redirect('/questions');
        }

        toastr()->error('An error has occurred please try again later.');

        return back();

        // return redirect('/questions');
    }
}
