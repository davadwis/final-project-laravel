<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
        ]);

        Answer::create([
            'content' => $request->content,
            'user_id' => Auth::user()->id,
            'question_id' => $id,
        ]);

        return redirect('/questions' . '/' . $id);
    }

    public function edit($id)
    {
        $answers = Answer::find($id);
        $questions = Question::get();

        return view('pages.answers.edit', ['answers' => $answers, 'questions' => $questions]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            // 'question_id' => 'required',
        ]);

        $answer = Answer::find($id);
        $question_id = $answer->question_id;
        $answer->content = $request->content;
        $answer->save();

        toastr()->success('Updated successfully!');


        return redirect('/questions' . '/' . $question_id);
    }

    public function destroy($id)
    {

        $answers = Answer::find($id);
        $answers->delete();

        if ($answers instanceof Model) {
            toastr()->success('Deleted successfully!');

            return redirect()->back();
        }

        return redirect('/questions');
    }
}
