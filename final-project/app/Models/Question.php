<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Question extends Model
{
    use HasFactory;
    protected $table = 'questions';
    protected $fillable = ['content', 'thumbnail', 'topic_id', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class, "user_id");
    }

    public function topic() {
        return $this->belongsTo(Topic::class, "topic_id");
    }

    public function answers() {
        return $this->hasMany(Answer::class, "question_id");
    }
}
